package se.teamunderdog.lab3;

import se.teamunderdog.model.PackageInfo;

public class SortHelper {

    public static int getPackageOrder(PackageInfo packageInfo){
        return packageInfo.getPackage().orderClass;
    }

}
