package se.teamunderdog.lab3;

import org.apache.commons.lang3.builder.CompareToBuilder;
import se.teamunderdog.lab1.Worker;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.starterkit.models.responses.GameResponse;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class WorkerLab3Alt2 extends Worker {

    Consumer<List<PackageInfo>> sorter;

    public WorkerLab3Alt2(String name) {
        super(name);
    }
    public WorkerLab3Alt2(String name, GameResponse gameInformation) {
        super(name, gameInformation);
        this.sorter = sorter;
    }

    public WorkerLab3Alt2(String name, Consumer<List<PackageInfo>> sorter) {
        super(name);
        this.sorter = sorter;
    }

    public static int compareByOrderVolumeWeight(PackageInfo p1, PackageInfo p2) {
        return new CompareToBuilder()
                .append(p2.getPackage().orderClass, p1.getPackage().orderClass)
                .append(p2.getVolume(), p1.getVolume())
                .append(p2.getPackage().weightClass, p1.getPackage().weightClass).toComparison();
    }

    public static int compareByOrderVolume(PackageInfo p1, PackageInfo p2) {
        return new CompareToBuilder()
                .append(p2.getPackage().orderClass, p1.getPackage().orderClass)
                .append(p2.getVolume(), p1.getVolume()).toComparison();
    }

    public static int compareByOrderMaxArea(PackageInfo p1, PackageInfo p2) {
        return new CompareToBuilder()
                .append(p2.getPackage().orderClass, p1.getPackage().orderClass)
                .append(p2.getArea(), p1.getArea())
                .toComparison();
    }

    public static int compareByVolume(PackageInfo p1, PackageInfo p2) {
        return new CompareToBuilder()
                .append(p2.getPackage().orderClass, p1.getPackage().orderClass)
                .append(p2.getVolume(), p1.getVolume()).toComparison();
    }

    public static int compareByMaxArea(PackageInfo p1, PackageInfo p2) {
        return new CompareToBuilder()
                .append(p2.getPackage().orderClass, p1.getPackage().orderClass)
                .append(p2.getArea(), p1.getArea())
                .toComparison();
    }
    public List<PackageInfo> sort(){
        List<PackageInfo> packageInfos = new ArrayList<>(getPackageInfos());


        packageInfos.sort(WorkerLab3Alt2::compareByOrderMaxArea);

        packageInfos.sort(Worker.compareByOrder
                .thenComparing(Worker.compareByMaxArea));

        //packageInfos.sort(WorkerLab3Alt2::compareByOrderVolumeWeight);

        /*Collections<PackageInfo>.sort(packageInfos, Comparator.comparing(o -> )
                .thenComparing(Report::getStudentNumber)
                .thenComparing(Report::getSchool));*/

        return packageInfos;
    }

    @Override
    public List<PackageInfo> createPackageList() {
        List<PackageInfo> packageInfos = new ArrayList<>(getPackageInfos());
        //Function<PackageInfo, Integer> getOrderClass = (p) -> p.getPackage().orderClass;

        /*packageInfos.sort(Worker.compareByOrder
                .thenComparing(Worker.compareByMaxArea));*/

        sorter.accept(packageInfos);


        return packageInfos;
    }
}
