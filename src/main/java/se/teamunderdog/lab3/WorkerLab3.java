package se.teamunderdog.lab3;

import se.teamunderdog.lab1.Worker;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.starterkit.models.responses.GameResponse;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WorkerLab3 extends Worker {
    public WorkerLab3(GameResponse gameInformation) {
        super("WorkerLab3", gameInformation);
    }

    public void startSorting(Comparator<PackageInfo> comparator) {
        //super.startSorting(sorter);
        this.sorter = comparator;
        System.out.println("comparator = " + comparator.getClass().getSimpleName());

        getOrderE().sort(comparator);
        getOrderD().sort(comparator);
        getOrderC().sort(comparator);
        getOrderB().sort(comparator);
        getOrderA().sort(comparator);

        //sort by order prio - highest (A=0) should be last packed

        //sort each order class by volume

        //sort by weight - heaviest should be first in each row


    }

    public List<PackageInfo> createPackageList() {

        System.out.println("");
        getCompletePackList().addAll(getOrderE());
        getCompletePackList().addAll(getOrderD());
        getCompletePackList().addAll(getOrderC());
        getCompletePackList().addAll(getOrderB());
        getCompletePackList().addAll(getOrderA());

        return getCompletePackList();
    }
}
