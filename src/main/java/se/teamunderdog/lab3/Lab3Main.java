package se.teamunderdog.lab3;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import se.teamunderdog.PrintStat;
import se.teamunderdog.lab1.Worker;
import se.teamunderdog.model.Box;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.model.PackageWrapper;
import se.teamunderdog.model.VehicleInfo;
import se.teamunderdog.starterkit.GameLayer;
import se.teamunderdog.starterkit.models.Package;
import se.teamunderdog.starterkit.models.PointPackage;
import se.teamunderdog.starterkit.models.responses.FetchResponse;
import se.teamunderdog.starterkit.models.responses.GameResponse;
import se.teamunderdog.starterkit.models.responses.SubmitResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static se.teamunderdog.lab1.Worker.compareByMaxArea;
import static se.teamunderdog.lab1.Worker.compareByOrder;
import static se.teamunderdog.lab1.Worker.compareByWeight;

public class Lab3Main {

    public static final String apiKey = "9d56ce55-e076-4225-b4c4-3af88b74dc50";
    public final String map = "training2";

    int x_axis_length = 10;
    int y_axis_length = 10;
    int z_axis_length = 10;
    static GameLayer gameLayer = null;
    GameResponse gameInformation = null;
    long start;
    private int score;
    SubmitResponse submitResponse = null;
    List<SubmitResponse> submitResponses = new ArrayList<>();
    public static void main(String[] args) {
        List<Comparator<PackageInfo>> comparators =
                List.of(compareByMaxArea, compareByWeight, compareByOrder);
        gameLayer = new GameLayer(apiKey);

        Lab3Main main = new Lab3Main();

        main.execute();

        /*for (Comparator<PackageInfo> comparator : comparators) {
            main.execute(comparator);
        }*/


    }

    private void execute(){
        Consumer<List<PackageInfo>> compareByOrderByMaxAreaByWeight  = packageInfoList -> packageInfoList.sort(compareByOrder
                .thenComparing(compareByMaxArea)
                .thenComparing(compareByWeight));

        Consumer<List<PackageInfo>> compareByOrderByMaxArea  = packageInfoList -> packageInfoList.sort(compareByOrder
                .thenComparing(compareByMaxArea));

        Consumer<List<PackageInfo>> compareByWeightByMaxArea  = packageInfoList -> packageInfoList.sort(compareByWeight
                .thenComparing(compareByMaxArea));

        Consumer<List<PackageInfo>> compareByWeightByMaxAreaByOrder  = packageInfoList -> packageInfoList.sort(compareByWeight
                .thenComparing(compareByMaxArea).thenComparing(compareByOrder));

        Consumer<List<PackageInfo>> compareByWeightByOrderByMaxArea  = packageInfoList -> packageInfoList.sort(compareByWeight
                .thenComparing(compareByOrder).thenComparing(compareByMaxArea));

        Consumer<List<PackageInfo>> compareByOrder  = packageInfoList -> packageInfoList.sort(Worker.compareByOrder);

        Consumer<List<PackageInfo>> compareByMaxArea  = packageInfoList -> packageInfoList.sort(Worker.compareByMaxArea);

        Consumer<List<PackageInfo>> compareByWeight  = packageInfoList -> packageInfoList.sort(Worker.compareByWeight);

        List<Worker> workers = List.of(
        new WorkerLab3Alt2("compareByWeightByOrderByMaxArea", compareByWeightByOrderByMaxArea),
                new WorkerLab3Alt2("compareByWeight", compareByWeight),
                new WorkerLab3Alt2("compareByWeightByMaxArea", compareByWeightByMaxArea),
                new WorkerLab3Alt2("compareByWeightByMaxAreaByOrder", compareByWeightByMaxAreaByOrder),
                new WorkerLab3Alt2("compareByOrder", compareByOrder),
                new WorkerLab3Alt2("compareByOrderByMaxAreaByWeight", compareByOrderByMaxAreaByWeight),
                new WorkerLab3Alt2("compareByOrderByMaxArea", compareByOrderByMaxArea)
                );
        String highScoreWorker = "";
        String higScoreLink = "";
        int badScore = -1;
        String badScoreWorker = "";
        String badScoreLink = "";

        for (Worker worker : workers){
            initGame();
            start = System.currentTimeMillis();
            worker.init(gameInformation);
            List<PackageInfo> packageList = worker.createPackageList();
            PrintStat.printBefore(gameInformation, new VehicleInfo(gameInformation.vehicle), worker);

            FetchResponse fetchResponse = start(packageList);

            if(fetchResponse != null && submitResponse != null){
                if (fetchResponse != null && fetchResponse.score > score){
                    highScoreWorker = worker.getName();
                    score = fetchResponse.score;
                    higScoreLink = submitResponse.link;
                }
                if(badScore == -1 || badScore > fetchResponse.score){
                    badScore = fetchResponse.score;
                    badScoreWorker = worker.getName();
                    badScoreLink = submitResponse.link;
                }
            }

        }
        System.out.println("");
        System.out.println("================================================== ");
        System.out.println("highScoreWorker = " + highScoreWorker);
        System.out.println("High score = " + score);
        System.out.println("higScoreLink = " + higScoreLink);
        System.out.println("");
        System.out.println("badScoreWorker = " + badScoreWorker);
        System.out.println("badScore = " + badScore);
        System.out.println("badScoreLink = " + badScoreLink);
        System.out.println("");
        System.out.println("workers = " + workers.stream().map(Worker::getName).collect(Collectors.toList()));
        System.out.println("");
        System.out.println("links = " + Optional.ofNullable(submitResponses).orElse(Collections.emptyList()).stream().map(submitResponse1 -> submitResponse1.link).collect(Collectors.toList()));
        System.out.println("================================================== ");


    }

    private void execute(Comparator<PackageInfo> comparator){
        initGame();

        start = System.currentTimeMillis();

        //Workers

        Worker worker = new WorkerLab3(gameInformation);
        worker.startSorting(comparator);
        List<PackageInfo> packageList = worker.createPackageList();

        PrintStat.printBefore(gameInformation, new VehicleInfo(gameInformation.vehicle), worker);

        FetchResponse fetchResponse = start(packageList);



    }

    private FetchResponse start(List<PackageInfo> packageList) {

        List<PackageWrapper> boxList = new ArrayList<>();
        List<PackageInfo> missed = new ArrayList<>();
        for (PackageInfo packageInfo : packageList) {
            Package p = packageInfo.getPackage();
            if (boxList.isEmpty()) {
                boxList.add(new PackageWrapper(p , createBox(0, 0, 0, p)));
            } else {
                PackageWrapper box = findFreePlace(boxList, p);
                if (box != null){
                    boxList.add(box);
                } else {
                    System.out.println("box is null for package " + p);
                    missed.add(packageInfo);
                }

            }

        }

        for (PackageInfo packageInfo : missed){
            Package p = packageInfo.getPackage();
            PackageWrapper box = findFreePlace(boxList, p);
            if (box != null){
                boxList.add(box);
            } else {
                System.out.println("box is null for missed package " + p);
            }


        }
        FetchResponse fetchResponse = submitGame(boxList);
        return fetchResponse;

    }


    /**
     * width = x
     * height = y
     * depth = z
     * @param boxList
     * @param p
     * @return
     */
    public PackageWrapper findFreePlace(List<PackageWrapper> boxList, Package p){
        for (int x = 0; x < x_axis_length; x++) {
            for (int y = 0; y < y_axis_length; y++) {
                for (int z = 0; z < z_axis_length; z++) {
                    Box box = createBox(x, y, z, p);
                    if (fitOnXYZAxis(box)){
                        Box boxCollision = isBoxCollision(boxList, box);
                        if (boxCollision != null) {
                            return new PackageWrapper(p, boxCollision);
                        }
                    } else {
                        //System.out.println("No fit on XYZ " +box);
                        //try rotate
                        /*Pair<PackageWrapper.RotatedDirection, Box> rotatedBox = rotate(boxList, box);
                        if (rotatedBox != null) {
                            PackageWrapper packageWrapper = new PackageWrapper(p, box);
                            packageWrapper.rotatedBox = rotatedBox.getRight();
                            packageWrapper.rotatedDirection = rotatedBox.getLeft();
                            return packageWrapper;
                        }
                        */
                        /*if (rotatedBox != null) {
                            Box firstNoCollisionBox = isBoxCollision(boxList, rotatedBox);
                            if (firstNoCollisionBox != null) {
                                PackageWrapper packageWrapper = new PackageWrapper(p, box);
                                packageWrapper.rotatedBox = rotatedBox;
                                return packageWrapper;
                            }
                        }*/
                    }
                }
            }
        }
        return null;
    }

    private Pair<PackageWrapper.RotatedDirection, Box> rotate(List<PackageWrapper> boxList, Box box) {
        Box rotatedBox = rotateBoxZToY(boxList, box, true);
        if (rotatedBox != null) {
            return new MutablePair<PackageWrapper.RotatedDirection, Box>(PackageWrapper.RotatedDirection.ZTOY, rotatedBox);
            //return rotatedBox;
        }

        /*rotatedBox = rotateBoxZToX(boxList, box, true);
        if (rotatedBox != null){
            return rotatedBox;
        }

        rotatedBox = rotateBoxXToY(boxList, box,true);
        if (rotatedBox != null){
            return rotatedBox;
        }

        rotatedBox = rotateBoxZToY(boxList, box, false);
        rotatedBox = rotateBoxXToY(boxList, rotatedBox,true);
        if (rotatedBox != null){
            return rotatedBox;
        }

        rotatedBox = rotateBoxZToX(boxList, box, false);
        rotatedBox = rotateBoxZToY(boxList, rotatedBox,true);
        if (rotatedBox != null){
            return rotatedBox;
        }*/
        return null;
    }

    private Box rotateBoxXToY(List<PackageWrapper> boxList, Box box, boolean checkFitOnXYZAxis){
        //int x = box.getMinY();
        //int y = box.getMinX();
        //int z = box.getMinZ();
        int x = box.getMinX();
        int y = box.getMinY();
        int z = box.getMinZ();
        int length = box.getWidth();
        int width = box.getLength();
        int height = box.getHeight();
        Box rotatedBox = getRotatedBox(boxList, checkFitOnXYZAxis, x, y, z, length, width, height);
        if (rotatedBox == null) return null;

        return rotatedBox;

    }

    private Box rotateBoxZToX(List<PackageWrapper> boxList, Box box, boolean checkFitOnXYZAxis) {
        //int x = box.getMinZ();
        //int y = box.getMinY();
        //int z = box.getMinX();
        int x = box.getMinX();
        int y = box.getMinY();
        int z = box.getMinZ();
        int length = box.getHeight();
        int width = box.getWidth();
        int height = box.getLength();
        Box rotatedBox = getRotatedBox(boxList, checkFitOnXYZAxis, x, y, z, length, width, height);
        if (rotatedBox == null) return null;
        return rotatedBox;
    }

    private Box rotateBoxZToY(List<PackageWrapper> boxList, Box box, boolean checkFitOnXYZAxis){
        //int x = box.getMinX();
        //int y = box.getMinZ();
        //int z = box.getMinY();
        int x = box.getMinX();
        int y = box.getMinY();
        int z = box.getMinZ();
        int length = box.getLength();
        int width = box.getHeight();
        int height = box.getWidth();
        Box rotatedBox = getRotatedBox(boxList, checkFitOnXYZAxis, x, y, z, length, width, height);
        return rotatedBox;

    }

    private Box getRotatedBox(List<PackageWrapper> boxList, boolean checkFitOnXYZAxis, int x, int y, int z, int length, int width, int height) {
        Box rotatedBox = new Box(x, y, z, width, height, length);
        if (checkFitOnXYZAxis && !fitOnXYZAxis(rotatedBox)) {
            return null;
        }
        if (checkFitOnXYZAxis) {
            Box firstNoCollisionBox = isBoxCollision(boxList, rotatedBox);
            if (firstNoCollisionBox == null) {
                return null;
            }
        }
        return rotatedBox;
    }

    private FetchResponse submitGame(List<PackageWrapper> boxList){
        ArrayList<PointPackage> solution = new ArrayList<>();
        for (PackageWrapper pw : boxList) {
            solution.add(addPackage(pw));
        }

        System.out.println("boxList.size() = " + boxList.size());
        System.out.println("solution.size() = " + solution.size());
        System.out.println("boxList = " + boxList);
        System.out.println("solution = " + solution);
        long stop = System.currentTimeMillis();

        try {
            submitResponse = gameLayer.SubmitGame(solution, map, apiKey);
            submitResponses.add(submitResponse);
            System.out.println("Your score is: " + submitResponse.score);
            System.out.println("The game id is: " + submitResponse.gameId);
            System.out.println( submitResponse.link);

            System.out.println("Time: = " + (stop - start));

            // Here you can use FetchGame(ApiKey, gameId) to look at a specific game
            FetchResponse aGame = gameLayer.FetchGame(apiKey, submitResponse.gameId);
            return aGame;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    private PointPackage addPackage(PackageWrapper packageWrapper){
        Package p = packageWrapper.p;
        int _xp = packageWrapper.box.getMinX(), _yp = packageWrapper.box.getMinY(), _zp = packageWrapper.box.getMinZ();

        /*if (packageWrapper.rotatedBox != null){

            new PointPackage(p.id, _xp, _xp, _xp, _xp, _xp + p.length, _xp + p.length,
                    _xp + p.length, _xp + p.length, _yp, _yp, _yp, _yp, _yp + p.width, _yp + p.width,
                    _yp + p.width, _yp + p.width, _zp, _zp, _zp, _zp, _zp + p.height, _zp + p.height,
                    _zp + p.height, _zp + p.height, p.weightClass, p.orderClass);
        }*/



        return new PointPackage(p.id, _xp, _xp, _xp, _xp, _xp + p.length, _xp + p.length,
                _xp + p.length, _xp + p.length, _yp, _yp, _yp, _yp, _yp + p.width, _yp + p.width,
                _yp + p.width, _yp + p.width, _zp, _zp, _zp, _zp, _zp + p.height, _zp + p.height,
                _zp + p.height, _zp + p.height, p.weightClass, p.orderClass);


    }

    private Box isBoxCollision(List<PackageWrapper> boxList, Box box){
        boolean boxCollide = boxList.stream()
                .map(PackageWrapper::getBox)
                .anyMatch(boundingBox -> boundingBox.intersects(box));
        if (!boxCollide){
            return box;
        }

        return null;
    }

    /*private Box rotateBoxZToY(Box box, int x, int y, int z, int length, int width, int height){

    }*/



    private boolean fitOnXYZAxis(Box box){
        return box.getMaxX() <= x_axis_length
                && box.getMaxY() <= y_axis_length
                && box.getMaxZ() <= z_axis_length;
    }

    private  Box createBox(int x, int y, int z, Package p){
        Box box = new Box(x, y, z, p.width, p.height, p.length);
        return box;
    }

    private void initGame() {

        gameInformation = gameLayer.newGame(map, apiKey);
        x_axis_length = gameInformation.vehicle.length;
        y_axis_length = gameInformation.vehicle.width;
        z_axis_length = gameInformation.vehicle.height;

    }

}