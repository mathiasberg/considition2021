package se.teamunderdog;

import se.teamunderdog.starterkit.models.Package;

import java.util.Comparator;

public class SortPackagesHelper {

    public static Comparator<Package> compareByOrder = new Comparator<Package>() {
        @Override
        public int compare(Package o1, Package o2) {
            return Integer.compare(o2.orderClass, o1.orderClass);
        }
    };

}
