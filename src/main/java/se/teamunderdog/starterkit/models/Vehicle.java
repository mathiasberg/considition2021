package se.teamunderdog.starterkit.models;

public class Vehicle {
    public int length;
    public int height;
    public int width;

    public Vehicle(int length, int height, int width){
        this.length= length;
        this.height = height;
        this.width = width;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "length=" + length +
                ", height=" + height +
                ", width=" + width +
                '}';
    }
}
