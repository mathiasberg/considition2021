package se.teamunderdog.starterkit.models.responses;

import se.teamunderdog.starterkit.models.Package;
import se.teamunderdog.starterkit.models.Vehicle;

import java.util.ArrayList;

public class GameResponse {
    public String mapName;
    public Vehicle vehicle;
    public ArrayList<Package> dimensions;

    public GameResponse(String mapName, Vehicle vehicle, ArrayList<Package> dimensions){
        this.mapName = mapName;
        this.vehicle = vehicle;
        this.dimensions = dimensions;
    }
}
