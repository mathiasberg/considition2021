package se.teamunderdog;

import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.model.VehicleInfo;
import se.teamunderdog.lab1.Worker;
import se.teamunderdog.lab1.WorkerLoadVehicle;
import se.teamunderdog.starterkit.GameLayer;
import se.teamunderdog.starterkit.models.PointPackage;
import se.teamunderdog.starterkit.models.responses.FetchResponse;
import se.teamunderdog.starterkit.models.responses.GameResponse;
import se.teamunderdog.starterkit.models.responses.SubmitResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * x=length
 * y=width
 * z=height
 *
 *
 */
public class Main {
    public static final String ApiKey = "9d56ce55-e076-4225-b4c4-3af88b74dc50"; //TODO Put your teams API Key here
    public static final String Map = "training1"; //TODO Enter what map you want to play,
     //new ones will be released on considition.com/rules

    public static void main(String[] args) {
        GameLayer gameLayer = new GameLayer(ApiKey);
        GameResponse gameInformation = gameLayer.newGame(Map, ApiKey);

        Worker worker = new Worker("DefaultWorker", gameInformation);
        List<PackageInfo> packageList = worker.createPackageList();

        PrintStat.printBefore(gameInformation, new VehicleInfo(gameInformation.vehicle), worker);

        //WorkerLoadVehicle workerLoadVehicle = new WorkerLoadVehicle(gameInformation, packageList);
        //ArrayList<PointPackage> solution = workerLoadVehicle.getSolution();
        GreedySolver greedySolver = new GreedySolver(gameInformation.dimensions, gameInformation.vehicle);
        //TODO Create your own solver with SubmitResponse as return value
        ArrayList<PointPackage> solution = greedySolver.Solve();
        SubmitResponse submitResponse = gameLayer.SubmitGame(solution, Map, ApiKey);


        System.out.println("Your score is: " + submitResponse.score);
        System.out.println("The game id is: " + submitResponse.gameId);
        System.out.println( submitResponse.link);

        // Here you can use FetchGame(ApiKey, gameId) to look at a specific game
        FetchResponse aGame = gameLayer.FetchGame(ApiKey, submitResponse.gameId);

        // Or just fetch them all in a list and then maybe compare them and find interesting ones
        ArrayList<FetchResponse> allYourGames = gameLayer.FetchGame(ApiKey);
    }
}
