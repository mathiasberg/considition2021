package se.teamunderdog.lab1;

import se.teamunderdog.Main;
import se.teamunderdog.PrintStat;
import se.teamunderdog.lab2.Lab2Main;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.model.VehicleInfo;
import se.teamunderdog.starterkit.GameLayer;
import se.teamunderdog.starterkit.models.PointPackage;

import java.util.ArrayList;
import java.util.List;

public class Lab1Main {
    public static void main(String[] args) {
        GameLayer gameLayer = new GameLayer(Main.ApiKey);
        se.teamunderdog.starterkit.models.responses.GameResponse gameInformation = gameLayer.newGame(Main.Map, Main.ApiKey);

        Worker worker = new Worker("DefaultWorker", gameInformation){
            @Override
            public List<PackageInfo> createPackageList() {
                return this.getCompletePackList();
            }
        };
        worker.startSorting();
        List<PackageInfo> packageList = worker.createPackageList();

        PrintStat.printBefore(gameInformation, new VehicleInfo(gameInformation.vehicle), worker);



        WorkerLoadVehicle workerLoadVehicle = new WorkerLoadVehicle(gameInformation, packageList);
        ArrayList<PointPackage> solution = workerLoadVehicle.getSolution();
        //GreedySolver2 greedySolver = new GreedySolver2(gameInformation.dimensions, gameInformation.vehicle);
        //TODO Create your own solver with SubmitResponse as return value
        //ArrayList<PointPackage> solution = greedySolver.solve();
        se.teamunderdog.starterkit.models.responses.SubmitResponse submitResponse = gameLayer.SubmitGame(solution, Main.Map, Main.ApiKey);


        System.out.println("Your score is: " + submitResponse.score);
        System.out.println("The game id is: " + submitResponse.gameId);
        System.out.println( submitResponse.link);
    }
}
