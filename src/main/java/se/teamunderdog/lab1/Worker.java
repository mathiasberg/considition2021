package se.teamunderdog.lab1;

import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.starterkit.models.Package;
import se.teamunderdog.starterkit.models.responses.GameResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Worker {

    private GameResponse gameInformation;
    private String name;

    protected Comparator<PackageInfo> sorter;

    private int totalPackagesVolume;
    private final List<PackageInfo> packageInfos = new ArrayList<>();

    private List<PackageInfo> orderA;
    private List<PackageInfo> orderB;
    private List<PackageInfo> orderC;
    private List<PackageInfo> orderD;
    private List<PackageInfo> orderE;

    private List<PackageInfo> completePackList = new ArrayList<>();

    private List<PackageInfo> weightHeavy, weightMedium, weightLight;

    public static Comparator<PackageInfo> compareByOrder = new Comparator<PackageInfo>() {
        @Override
        public int compare(PackageInfo o1, PackageInfo o2) {
            return Integer.compare(o2.getPackage().orderClass, o1.getPackage().orderClass);
        }
    };

    public static Comparator<PackageInfo> compareByWeight = new Comparator<PackageInfo>() {
        @Override
        public int compare(PackageInfo o1, PackageInfo o2) {
            return Integer.compare(o2.getPackage().weightClass, o1.getPackage().weightClass);
        }
    };

    public static Comparator<PackageInfo> compareByMaxArea = new Comparator<PackageInfo>() {
        @Override
        public int compare(PackageInfo o1, PackageInfo o2) {
            return (o2.getPackage().width * o2.getPackage().length - o1.getPackage().width * o1.getPackage().length);
        }
    };

    public Worker(String name){
        this.name = name;
    }

    public Worker(String name, GameResponse gameInformation) {
        this.gameInformation = gameInformation;
        this.name = name;

        init(gameInformation);

    }

    public void init(GameResponse gameInformation){
        this.gameInformation = gameInformation;
        this.gameInformation.dimensions.forEach(aPackage -> packageInfos.add(new PackageInfo(aPackage)));

        this.totalPackagesVolume = packageInfos.stream().mapToInt(PackageInfo::getVolume).sum();

        orderA = createByFilter(aPackage -> aPackage.orderClass == 0);
        orderB = createByFilter(aPackage -> aPackage.orderClass == 1);
        orderC = createByFilter(aPackage -> aPackage.orderClass == 2);
        orderD = createByFilter(aPackage -> aPackage.orderClass == 3);
        orderE = createByFilter(aPackage -> aPackage.orderClass == 4);

        weightLight = createByFilter(aPackage -> aPackage.weightClass == 0);
        weightMedium = createByFilter(aPackage -> aPackage.weightClass == 1);
        weightHeavy = createByFilter(aPackage -> aPackage.weightClass == 2);

        completePackList.addAll(orderE);
        completePackList.addAll(orderD);
        completePackList.addAll(orderC);
        completePackList.addAll(orderB);
        completePackList.addAll(orderA);
    }

    protected List<PackageInfo> createByFilter(Predicate<Package> filter) {
        return this.gameInformation.dimensions.stream()
                .filter(filter::test)
                .map(PackageInfo::new)
                .collect(Collectors.toList());
    }
    public void startSorting() {
        sorter = compareByMaxArea;
        startSorting(sorter);

    }
    public void startSorting(Comparator<PackageInfo> comparator) {
        sorter = comparator;
        System.out.println("comparator = " + comparator.getClass());

        Collections.sort(orderE, comparator);
        Collections.sort(orderD, comparator);
        Collections.sort(orderC, comparator);
        Collections.sort(orderB, comparator);
        Collections.sort(orderA, comparator);

        //sort by order prio - highest (A=0) should be last packed

        //sort each order class by volume

        //sort by weight - heaviest should be first in each row


    }

    public List<PackageInfo> createPackageList() {
        return this.createPackageList();
        //startSorting();

        /*System.out.println("");
        completePackList.addAll(orderE);
        completePackList.addAll(orderD);
        completePackList.addAll(orderC);
        completePackList.addAll(orderB);
        completePackList.addAll(orderA);
        this.createPackageList();
        return completePackList;*/
    }

    public List<PackageInfo> getOrderA() {
        return orderA;
    }

    public List<PackageInfo> getOrderB() {
        return orderB;
    }

    public List<PackageInfo> getOrderC() {
        return orderC;
    }

    public List<PackageInfo> getOrderD() {
        return orderD;
    }

    public List<PackageInfo> getOrderE() {
        return orderE;
    }

    public List<PackageInfo> getWeightHeavy() {
        return weightHeavy;
    }

    public List<PackageInfo> getWeightMedium() {
        return weightMedium;
    }

    public List<PackageInfo> getWeightLight() {
        return weightLight;
    }

    public List<PackageInfo> getCompletePackList() {
        return completePackList;
    }

    public int getTotalPackagesVolume() {
        return totalPackagesVolume;
    }

    public String getName() {
        return name;
    }

    public Comparator<PackageInfo> getSorter() {
        return sorter;
    }

    public List<PackageInfo> getPackageInfos() {
        return packageInfos;
    }


}
