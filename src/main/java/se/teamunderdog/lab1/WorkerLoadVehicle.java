package se.teamunderdog.lab1;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.model.VehicleInfo;
import se.teamunderdog.starterkit.models.Package;
import se.teamunderdog.starterkit.models.PointPackage;
import  se.teamunderdog.starterkit.models.responses.GameResponse;

import java.util.ArrayList;
import java.util.List;

public class WorkerLoadVehicle {

    private final GameResponse gameInformation;
    private final VehicleInfo vehicleInfo;
    private int lastKnownMaxLength;
    private int lastKnownMaxWidth;
    private ArrayList<PointPackage> solution = new ArrayList<>();
    int xAxis = 0;
    int yAxis = 0;
    int zAxis = 0;

    public WorkerLoadVehicle(GameResponse gameInformation, List<PackageInfo> originalPackageList) {
        this.gameInformation = gameInformation;
        vehicleInfo = new VehicleInfo(this.gameInformation.vehicle);

        List<PackageInfo> packageInfoList = new ArrayList<>(originalPackageList);

        PackageInfo parentLocatedPackage = null;
        PackageInfo currentPackage = null;


        List<PackageInfo> listX = new ArrayList<>();
        List<PackageInfo> listY = new ArrayList<>();
        List<PackageInfo> listZ = new ArrayList<>();

        List<PackageInfo> packingStackNow = new ArrayList<>();
        List<PackageInfo> prevStack = new ArrayList<>();

        boolean packingZ = false;
        boolean packingY = false;
        boolean packingX = false;

        while (!packageInfoList.isEmpty()) {
            for (PackageInfo packageToFit : originalPackageList) {

                if(doesPackageFitZ(packageToFit)){
                    packingZ = true;
                    if(packingY || packingX){
                        //prevStack = new ArrayList<>(packingStackNow);
                        packingStackNow = new ArrayList<>();
                    }

                    //if last packing was y
                    /*if(packingY){
                        int newZAxis = zAxis + packageToFit.getHeight();
                        int prevStackZAxis = 0;
                        for (PackageInfo p : prevStack) {
                            prevStackZAxis += p.getHeight();
                            if(prevStackZAxis >= zAxis && prevStackZAxis <= newZAxis){
                                yAxis += p.getWidth();
                            }
                        }
                    }*/

                    addPackageToSolution(packageToFit, xAxis, yAxis, zAxis);

                    zAxis += packageToFit.getHeight();
                    packageToFit.setPlots(parentLocatedPackage);

                    vehicleInfo.loadPackage(packageToFit);

                    parentLocatedPackage = packageToFit;
                    packageInfoList.remove(packageToFit);

                    listZ.add(packageToFit);
                    listY = new ArrayList<>();
                    listX = new ArrayList<>();
                    packingY = false;
                    packingX = false;

                } else if (doesPackageFitY(packageToFit)) {
                    packingY = true;
                    if(packingZ || packingX){
                        prevStack = new ArrayList<>(packingStackNow);
                        packingStackNow = new ArrayList<>();
                        packingZ = false;
                        packingX = false;
                    }
                    //int lastWidth = prevStack.stream().findFirst().map(PackageInfo::getWidth).orElse(lastKnownMaxWidth);
                    //yAxis += lastWidth;
                    yAxis += lastKnownMaxWidth;
                    zAxis = 0;

                    addPackageToSolution(packageToFit, xAxis, yAxis, zAxis);

                    zAxis = packageToFit.getHeight();
                    lastKnownMaxWidth = 0;

                    vehicleInfo.loadPackage(packageToFit);
                    parentLocatedPackage = packageToFit;
                    packageInfoList.remove(packageToFit);

                    listY.add(packageToFit);
                    listX = new ArrayList<>();
                    listZ = new ArrayList<>();

                } else if (doesPackageFitX(packageToFit)) {
                    packingX = true;
                    if(packingZ || packingY){
                        prevStack = new ArrayList<>(packingStackNow);
                        packingStackNow = new ArrayList<>();
                        packingZ = false;
                        packingY = false;
                    }
                    xAxis += lastKnownMaxLength;

                    //xAxis += packageToFit.getWidth();
                    yAxis = 0;
                    zAxis = 0;
                    addPackageToSolution(packageToFit, xAxis, yAxis, zAxis);

                    zAxis = packageToFit.getHeight();
                    lastKnownMaxLength = 0;

                    vehicleInfo.loadPackage(packageToFit);
                    parentLocatedPackage = packageToFit;
                    packageInfoList.remove(packageToFit);

                    listX.add(packageToFit);
                    listY = new ArrayList<>();
                    listZ = new ArrayList<>();

                } else {
                    System.out.println("Something went wrong! Package wont fit ");
                    throw new RuntimeException("Something went wrong! Package wont fit ");
                }
                packingStackNow.add(packageToFit);
                //}
                setMaxX(packageToFit.getPackage());
                setMaxY(packageToFit.getPackage());





                //place package near last placed package
                /*f (parentLocatedPackage == null) {
                    //set plot for current, based on parent
                    packageToFit.setPlots(parentLocatedPackage);

                    vehicleInfo.loadPackage(packageToFit);
                    parentLocatedPackage = packageToFit;
                    packageInfoList.remove(packageToFit);

                } else {*/
                    //check if fit on z-axis (height)

            }


        }

    }

    private boolean doesPackageFitX(PackageInfo p) {
        return (xAxis + lastKnownMaxLength + p.getLength() <= vehicleInfo.getLength());
    }

    private boolean doesPackageFitY(PackageInfo p) {
        return (yAxis + lastKnownMaxWidth + p.getWidth() <= vehicleInfo.getWidth() &&
                xAxis + p.getLength() < vehicleInfo.getLength());
    }

    private boolean doesPackageFitZ(PackageInfo p) {
        return (xAxis + p.getLength() <= vehicleInfo.getLength() &
                yAxis + p.getWidth() <= vehicleInfo.getWidth() &
                zAxis + p.getHeight() <= vehicleInfo.getHeight());
    }

    private void addPackageToSolution(PackageInfo packageToFit, int _xp, int _yp, int _zp){
        packageToFit.setPlacedXAxis(_xp);
        packageToFit.setPlacedYAxis(_yp);
        packageToFit.setPlacedZAxis(_zp);
        Package p = packageToFit.getPackage();
        solution.add(new PointPackage(p.id, _xp, _xp, _xp, _xp, _xp + p.length, _xp + p.length,
                _xp + p.length, _xp + p.length, _yp, _yp, _yp, _yp, _yp + p.width, _yp + p.width,
                _yp + p.width, _yp + p.width, _zp, _zp, _zp, _zp, _zp + p.height, _zp + p.height,
                _zp + p.height, _zp + p.height, p.weightClass, p.orderClass));
    }

    public ArrayList<PointPackage> getSolution() {
        return solution;
    }

    private void setMaxX(Package p) {
        if (p.length > lastKnownMaxLength) {
            lastKnownMaxLength = p.length;
        }
    }

    private void setMaxY(Package p) {
        if (p.width > lastKnownMaxWidth) {
            lastKnownMaxWidth = p.width;
        }
    }
    /*

    private boolean doesPackageFitY(Package p) {
        return (_yp + _lastKnownMaxWidth + p.width < truckY &
                _xp + p.length < truckX);
    }

    private boolean doesPackageFitZ(PackageInfo p, PackageInfo parent) {
        return (parent.getTopPlot().getZ() + p.getLength() <= vehicleInfo.getLength() &
                _yp + p.width < truckY &
                _zp + p.height < truckZ);
    }*/
}

