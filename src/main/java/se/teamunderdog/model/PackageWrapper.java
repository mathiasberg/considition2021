package se.teamunderdog.model;
import se.teamunderdog.model.Box;
import se.teamunderdog.starterkit.models.Package;

public class PackageWrapper {

    public enum RotatedDirection {
        ZTOY, ZTOX
    }

    public Package p;
    public Box box;
    public Box rotatedBox;
    public RotatedDirection rotatedDirection;

    public PackageWrapper(Package p, Box box) {
        this.p = p;
        this.box = box;
    }

    public Package getP() {
        return p;
    }

    public Box getBox() {
        return box;
    }

    @Override
    public String toString() {
        return "PackageWrapper{" +
                "p=" + p +
                ", box=" + box +
                '}';
    }
}
