package se.teamunderdog.model;

import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.starterkit.models.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class VehicleInfo {

    private final int area;
    private final int volume;
    private final Vehicle vehicle;
    private int currentFreeVolumeSpace;
    private List<PackageInfo> currentLoadedPackages = new ArrayList<>();

    public VehicleInfo(Vehicle vehicle) {
        this.vehicle = vehicle;
        this.area = vehicle.width * vehicle.length;
        this.volume = vehicle.length * vehicle.width * vehicle.height;

        this.currentFreeVolumeSpace = this.volume;
    }

    public boolean checkPackageFitLength(PackageInfo packageToFit, int parentX){
        return parentX + packageToFit.getLength() <= getLength();
    }

    public boolean checkPackageFitWidth(PackageInfo packageToFit, int parentY){
        return parentY + packageToFit.getWidth() <= getWidth();
    }

    public boolean checkPackageFitHeight(PackageInfo packageToFit, int parentZ){
        return parentZ + packageToFit.getHeight() <= getHeight();
    }

    public boolean checkPackageFitZ(PackageInfo packageToFit, PackageInfo parent){
        return (parent.getTopPlot().getX() + packageToFit.getLength() <= getLength() &&
                parent.getTopPlot().getY() + packageToFit.getWidth() <= getWidth() &&
                parent.getTopPlot().getZ() + packageToFit.getHeight() <= getHeight());
    }

    public int getLength(){
        return vehicle.length;
    }

    public int getHeight(){
        return vehicle.height;
    }

    public int getWidth(){
            return vehicle.width;
    }

    public void setCurrentFreeVolumeSpace(int currentFreeVolumeSpace) {
        this.currentFreeVolumeSpace = currentFreeVolumeSpace;
    }

    public void setCurrentLoadedPackages(List<PackageInfo> currentLoadedPackages) {
        this.currentLoadedPackages = currentLoadedPackages;
    }

    public int getArea() {
        return area;
    }

    public int getVolume() {
        return volume;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public int getCurrentFreeVolumeSpace() {
        return currentFreeVolumeSpace;
    }

    public List<PackageInfo> getCurrentLoadedPackages() {
        return currentLoadedPackages;
    }

    public void loadPackage(PackageInfo packageToLoad) {
        currentLoadedPackages.add(packageToLoad);
        this.currentFreeVolumeSpace -= packageToLoad.getVolume();
    }
}
