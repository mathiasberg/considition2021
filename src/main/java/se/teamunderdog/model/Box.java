package se.teamunderdog.model;

/**
 * javafx
 * widht : MaxX
 * height : MaxY
 * depth : MaxZ
 *
 * considition
 * length = minX
 * width : minY
 * height = minZ
 *
 */
public class Box {

    int minX, minY, minZ;
    int maxX, maxY, maxZ;
    int width, height, length;

    public Box(int x, int y, int z, int width, int height, int length) {
        this.minX = x;
        this.minY = y;
        this.minZ = z;
        this.width = width;
        this.height = height;
        this.length = length;
        this.maxX = minX + length;
        this.maxY = minY + width;
        this.maxZ = minZ + height;
    }

    public boolean isEmpty() {
        return this.getMaxX() < this.getMinX() || this.getMaxY() < this.getMinY() || this.getMaxZ() < this.getMinZ();
    }

    public boolean intersects(Box var1) {
        return var1 != null && !var1.isEmpty() ? this.intersectsMod(var1.getMinX(), var1.getMinY(), var1.getMinZ(), var1.getWidth(), var1.getHeight(), var1.length) : false;
    }


    public boolean intersectsMod(double minX, double minY, double minZ, double width, double height, double length) {
        if (!isEmpty() && !(width < 0) && !(height < 0) && !(length < 0)) {
            return minX + length > this.getMinX() && minY + width > this.getMinY() && minZ + height > this.getMinZ() && minX < this.getMaxX() && minY < this.getMaxY() && minZ < this.getMaxZ();
        } else {
            return false;
        }
    }

    public boolean intersects(double minX, double minY, double minZ, double width, double height, double length) {
        if (!isEmpty() && !(width < 0) && !(height < 0) && !(length < 0.)) {
            return minX + length >= this.getMinX() && minY + width >= this.getMinY() && minZ + height >= this.getMinZ() && minX <= this.getMaxX() && minY <= this.getMaxY() && minZ <= this.getMaxZ();
        } else {
            return false;
        }
    }

    //public boolean intersects(double var1, double var3, double var5, double var7, double var9, double var11)
    public boolean intersects2(double minX, double minY, double minZ, double width, double height, double depth) {
        if (!isEmpty() && !(width < 0.0D) && !(height < 0.0D) && !(depth < 0.0D)) {
            return minX + width >= this.getMinX() && minY + height >= this.getMinY() && minZ + depth >= this.getMinZ() && minX <= this.getMaxX() && minY <= this.getMaxY() && minZ <= this.getMaxZ();
        } else {
            return false;
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getLength() {
        return length;
    }

    public int getMinX() {
        return minX;
    }

    public int getMinY() {
        return minY;
    }

    public int getMinZ() {
        return minZ;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public int getMaxZ() {
        return maxZ;
    }

    @Override
    public String toString() {
        return "Box{" +
                "minX=" + minX +
                ", minY=" + minY +
                ", minZ=" + minZ +
                ", maxX=" + maxX +
                ", maxY=" + maxY +
                ", maxZ=" + maxZ +
                ", width=" + width +
                ", height=" + height +
                ", length=" + length +
                '}';
    }
}
