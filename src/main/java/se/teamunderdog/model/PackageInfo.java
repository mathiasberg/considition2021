package se.teamunderdog.model;

import se.teamunderdog.starterkit.models.Package;

import java.util.ArrayList;
import java.util.List;


public class PackageInfo {
    private final Package aPackage;
    private Package rotatedPackage;


    private  List<Plot> bottomPlots = new ArrayList<>();
    private  List<Plot> topPlots = new ArrayList<>() ;
    private final int volume;
    private final int area;
    private int placedXAxis;
    private int placedYAxis;
    private int placedZAxis;

    private int x1, x2, x3, x4, x5, x6, x7, x8;
    private int y1, y2, y3, y4, y5, y6, y7, y8;
    private int z1, z2, z3, z4, z5, z6, z7, z8;

    public PackageInfo(Package aPackage) {
        this.aPackage = aPackage;
        this.volume = aPackage.length * aPackage.width * aPackage.height;
        this.area = aPackage.width * aPackage.length;
    }

    public int getPlacedXAxis() {
        return placedXAxis;
    }

    public int getPlacedYAxis() {
        return placedYAxis;
    }

    public int getPlacedZAxis() {
        return placedZAxis;
    }

    public PackageInfo setPlacedXAxis(int placedXAxis) {
        this.placedXAxis = placedXAxis;
        return this;
    }

    public PackageInfo setPlacedYAxis(int placedYAxis) {
        this.placedYAxis = placedYAxis;
        return this;
    }

    public PackageInfo setPlacedZAxis(int placedZAxis) {
        this.placedZAxis = placedZAxis;
        return this;
    }

    public void setPlots(PackageInfo parent){
        //x1=parent.x1;x2=parent.x2;x3=parent.x3;x4=parent.x4;
        //x5=parent.x5+parent.getPackage().length;
        int parentLength = parent == null ? 0 : parent.getPackage().length;
        int parentWidth = parent == null ? 0 : parent.getPackage().width;
        int parentHeight = parent == null ? 0 : parent.getPackage().height;

        if (parent == null) {
            Package a = new Package(-1, 0, 0, 0, 0, 0);
            parent = new PackageInfo(a);

        }

        bottomPlots = List.of(
                new Plot(parent.x5, parent.y5, parent.z5),
                new Plot(parent.x6, parent.y6, parent.z6),
                new Plot(parent.x7, parent.y7, parent.z7),
                new Plot(parent.x8, parent.y8, parent.z8)
        );

        topPlots = List.of(
                new Plot(parent.x5 + parentLength, parent.y5 + parentWidth, parent.z5 + parentHeight),
                new Plot(parent.x6 + parentLength, parent.y6 + parentWidth, parent.z6 + parentHeight),
                new Plot(parent.x7 + parentLength, parent.y7 + parentWidth, parent.z7 + parentHeight),
                new Plot(parent.x8 + parentLength, parent.y8 + parentWidth, parent.z8 + parentHeight)
        );

    }

    public int getLength(){
        return aPackage.length;
    }

    public int getHeight(){
        return aPackage.height;
    }

    public int getWidth(){
        return aPackage.width;
    }

    public List<Plot> getBottomPlots() {
        return bottomPlots;
    }

    public Plot getTopPlot() {
        return topPlots.stream().findFirst().orElse(null);
    }

    public List<Plot> getTopPlots() {
        return topPlots;
    }

    public Package getPackage() {
        return aPackage;
    }

    public int getVolume() {
        return volume;
    }

    public int getArea() {
        return area;
    }

    public Package getRotatedPackage() {
        return rotatedPackage;
    }

    public PackageInfo setRotatedPackage(Package rotatedPackage) {
        this.rotatedPackage = rotatedPackage;
        return this;
    }

    @Override
    public String toString() {
        return "PackageInfo{" +
                "aPackage=" + aPackage +
                ", rotatedPackage=" + rotatedPackage +
                ", volume=" + volume +
                ", area=" + area +
                '}';
    }
}
