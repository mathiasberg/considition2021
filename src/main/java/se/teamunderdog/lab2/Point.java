package se.teamunderdog.lab2;

import se.teamunderdog.model.Plot;
import se.teamunderdog.starterkit.models.Package;
import se.teamunderdog.starterkit.models.PointPackage;

public class Point {
    public int x, y, z;
    public int id;
    public Point parent;

    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point(int id, int x, int y, int z) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point(int id, int x, int y, int z, Point parent) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.z = z;
        this.parent = parent;

    }

    public Point(Package p) {
        this.x = p.length;
        this.y = p.width;
        this.z = p.height;
    }

    @Override
    public String toString() {
        return "{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
