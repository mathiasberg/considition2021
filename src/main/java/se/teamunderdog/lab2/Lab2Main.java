package se.teamunderdog.lab2;

import se.teamunderdog.Main;
import se.teamunderdog.lab1.Worker;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.starterkit.GameLayer;
import se.teamunderdog.starterkit.models.Package;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Lab2Main {
    static int x_axis_length = 10;
    static int y_axis_length = 10;
    static int z_axis_length = 10;
    static ArrayList<ArrayList<ArrayList<String>>> space;
    static List<PackageInfo> loaded = new ArrayList<>();
    static List<PackageInfo> notLoaded = new ArrayList<>();

    public static void main2(String[] args){
        String[][][] array3d = new String[10][10][10];

        for(String [] [] array2d : array3d){
            for(String[] array : array2d){

            }
        }

        System.out.println(Arrays.deepToString(array3d));
    }

    private void searchFirstEmpty(String[][][] array3d){
        for(String [][] array2d : array3d){
            for(String[] array : array2d){

            }
        }
    }

    private static void print(String[][][] array3d){
        for(String [] [] array2d : array3d){
            for(String[] array : array2d){
                Arrays.fill(array, "hello");
            }
        }
    }
    public static void main(String[] args) {
        GameLayer gameLayer = new GameLayer(Main.ApiKey);
        se.teamunderdog.starterkit.models.responses.GameResponse gameInformation = gameLayer.newGame(Main.Map, Main.ApiKey);
        Lab2Main main = new Lab2Main();
        main.start(gameInformation);
    }

    public void start(se.teamunderdog.starterkit.models.responses.GameResponse gameInformation) {
        x_axis_length = gameInformation.vehicle.length;
        y_axis_length = gameInformation.vehicle.width;
        z_axis_length = gameInformation.vehicle.height;

        Worker worker = new WorkerLab2(gameInformation);
        worker.startSorting();
        List<PackageInfo> packageList = worker.createPackageList();

        space = new ArrayList<>(x_axis_length);

        for (int x = 0; x < x_axis_length; x++) {
            space.add(new ArrayList<ArrayList<String>>(y_axis_length));
            for (int y = 0; y < y_axis_length; y++) {
                space.get(x).add(new ArrayList<String>(z_axis_length));
                for (int z = 0; z < z_axis_length; z++) {
                    //space.get(x).get(y).add(new ArrayList<String>(z_axis_length));
                    space.get(x).get(y).add(z,"");
                }
            }
        }

        /*List<PackageInfo> packageList = new ArrayList<>();
        packageList.add(new PackageInfo(new Package(1, 2, 2, 3,1, 1)));
        packageList.add(new PackageInfo(new Package(2, 2, 2, 3,1, 1)));
        packageList.add(new PackageInfo(new Package(3, 2, 2, 5,1, 1)));
        packageList.add(new PackageInfo(new Package(4, 2, 2, 2,1, 1)));*/


        for (PackageInfo p : packageList){
            List<Integer> freeZPath = new ArrayList<>();

            //Always start from zero for each package
            Point findAtPoint = new Point(0, 0, 0);
            Point findAtPointPrev = new Point(0, 0, 0);
            while (freeZPath.size() < p.getHeight() && p.getRotatedPackage() == null){
                if (findAtPoint == null){
                    System.out.println("package = " + p);
                    System.out.println("start from = " + findAtPoint);
                    System.out.println("free spots = " + freeZPath);
                    throw new RuntimeException("Error could not find a next free spot");
                }
                findAtPointPrev = new Point(findAtPoint.x, findAtPoint.y, findAtPoint.z);

                findAtPoint = findFirstFreeZ(findAtPoint);
                freeZPath = findFreeZ(findAtPoint);

                //check if package fit on height, otherwise try rotate or get a new spot to place the package.
                if (p.getHeight() > freeZPath.size()) {
                    //check if package fit on width instead
                    if(p.getWidth() <= freeZPath.size() && p.getLength() <= y_axis_length){
                        //we need to rotate the package to fit on height.
                        System.out.println("Rotate!!");
                        Package rp = new Package(p.getPackage().id, p.getLength(), p.getHeight(), p.getWidth(),
                                p.getPackage().weightClass, p.getPackage().orderClass);
                        p.setRotatedPackage(rp);

                    } else {
                        //try find next free spot on yxis instead
                        if(p.getWidth() <= y_axis_length){
                            findAtPoint = findNextOnYAxis(findAtPoint, p);
                            if(findAtPoint != null){
                                freeZPath = findFreeZ(findAtPoint);
                            }
                        }
                    }
                }
                //check if we did not find any freeZPath on yaxis. then try on xaxis instead
                if(findAtPoint == null){
                    System.out.println("findAtPointPrev = " + findAtPointPrev);
                    findAtPoint = findNextOnXAxis(findAtPointPrev, p);
                    if(findAtPoint != null){
                        List<Integer> freeZPathOnNextXAxis = findFreeZ(findAtPoint);
                        freeZPath = freeZPathOnNextXAxis;
                        //check so we dont load higher on next x-axis
                        if(freeZPathOnNextXAxis.size() < freeZPath.size()){
                            System.out.println("!!!! Warning freeZPath = " + freeZPath);
                            System.out.println("freeZPathOnNextXAxis = " + freeZPathOnNextXAxis);
                        }
                    }
                }
            }


            System.out.println("package = " + p);
            System.out.println("start from = " + findAtPoint);
            System.out.println("free spots = " + freeZPath.size());

            //Point firstFreeZ = findFirstFreeZ(findAtPoint);
            Point firstFreeZ = findAtPoint;

            if(fitOnXYZAxis(firstFreeZ, p)) {
                traversAndFill(firstFreeZ, p);
                loaded.add(p);
            } else {
                System.out.println("ERROR package did not fit!!! + " + p);
                notLoaded.add(p);
            } /*else if(yAxisFit && xAxisFit) {
                Point nextFirstFreeZ = findNextOnYAxis(firstFreeZ);
                if(nextFirstFreeZ != null && fitOnXYZAxis(nextFirstFreeZ, p)){
                    traversAndFill(nextFirstFreeZ, p.getLength() , p.getWidth(), p.getHeight());
                    loaded.add(p);
                }

            } else if(xAxisFit) {
                Point nextFirstFreeZ = findNextOnXAxis(firstFreeZ);
            } else {
                Point secondFreeZ = findFirstFreeZ(firstFreeZ);
            }*/


        }


        Point firstFreeZ = findFirstFreeZ();
        List<Integer> freeZ = findFreeZ(firstFreeZ);
        //System.out.println("firstFreeZ = " + firstFreeZ);
        //System.out.println("freeZ = " + freeZ);
        System.out.println("map = " + space.get(0).get(0));
        System.out.println("loaded  = " + loaded.size());
        System.out.println("notloaded  = " + notLoaded.size());

    }

    private static Point findNextOnZAxis(Point point) {
        if(point.z + 1 < z_axis_length){
            return findFirstFreeZ(new Point(point.x, point.y, point.z +1));
        }
        return null;
    }

    private static Point findNextOnXAxis(Point point, PackageInfo p) {
        int minZ = z_axis_length;
        if(point.x + 1 < x_axis_length && point.y + 1 < y_axis_length){
            for (int i = 0;  i < p.getWidth(); i++){
                List<Integer> freeZ1 = findFreeZ(new Point(point.x +1, point.y + i, point.z));
                minZ = Math.min(freeZ1.size(), minZ);
            }
            //return findFirstFreeZ(new Point(point.x +1, 0, 0));
        }
        if(minZ >= p.getHeight()) {
            return findFirstFreeZ(new Point(point.x +1, point.y, point.z));
        }
        return null;
    }

    private static Point findNextOnYAxis(Point point, PackageInfo p){
        int minZ = z_axis_length;;
        if(point.y + 1 < y_axis_length && point.x + 1 < x_axis_length){
            for (int i = 0;  i < p.getWidth(); i++){
                List<Integer> freeZ1 = findFreeZ(new Point(point.x, point.y + i, point.z));
                minZ = Math.min(freeZ1.size(), minZ);
            }
        }
        if(minZ >= p.getHeight()) {
            return findFirstFreeZ(new Point(point.x, point.y +1, point.z));
        }
        return null;
    }

    private static Point findNextOnYAxis(Point point){
        if(point.y + 1 < y_axis_length){
            return findFirstFreeZ(new Point(point.x, point.y +1, 0));
        }
        return null;
    }

    private static boolean fitOnXYZAxis(Point point, PackageInfo p){
        if (p.getRotatedPackage() != null) {
            boolean xAxisFit = point.x + p.getRotatedPackage().length <= x_axis_length;
            boolean yAxisFit = point.y + p.getRotatedPackage().width <= y_axis_length;
            boolean zAxisFit = point.z + p.getRotatedPackage().height <= z_axis_length;
            return xAxisFit && yAxisFit && zAxisFit;
        } else {
            boolean xAxisFit = point.x + p.getLength() <= x_axis_length;
            boolean yAxisFit = point.y + p.getWidth() <= y_axis_length;
            boolean zAxisFit = point.z + p.getHeight() <= z_axis_length;
            return xAxisFit && yAxisFit && zAxisFit;
        }
    }



    private static void traversAndFill(Point start, PackageInfo p){
        //int[] firstFreeZ = findFirstFreeZ();
        //check free space up to height.
        List<Integer> freeZ = findFreeZ(start);



        Package pToLoad = p.getPackage();
        if (p.getRotatedPackage() != null ){
            pToLoad = p.getRotatedPackage();
        }

        fillSpace(pToLoad, start);
    }

    private static void fillSpace(Package p, Point start) {
        int xAxis = start.x;
        int yAxis = start.y;
        int zAxis = start.z;
        for (int x = xAxis; x < xAxis + p.length; x++) {
            for (int y = yAxis; y < yAxis + p.width; y++) {
                for (int z = zAxis; z < zAxis + p.height; z++) {
                    space.get(x).get(y).add(z,"x");
                }
            }
        }
    }

    /*private static List<Integer> findFreeY(int[] path, int width){
        int xAxis = path[0];
        int yAxis = path[1];
        int zAxis = path[2];
        List<Integer> zPath = new ArrayList<>();
        ArrayList<String> zAxisSpaces = space.get(xAxis).get(yAxis);
        for (int z = zAxis; z < z_axis_length; z++) {

        }
        for (int y = yAxis; y < y_axis_length; y++) {
            ArrayList<String> zSpaces = space.get(xAxis).get(y);
            if (zSpace.equals("")) {
                zPath.add(z);
            } else {
                return zPath;
            }
        }
        return zPath;

    }*/

    private static List<Integer> findFreeZ(Point start){
        int xAxis = start.x;
        int yAxis = start.y;
        int zAxis = start.z;
        List<Integer> zPath = new ArrayList<>();
        //ArrayList<String> zAxisSpaces = space.get(xAxis).get(yAxis);

        for (int z = zAxis; z < z_axis_length; z++) {
            String zSpace = space.get(xAxis).get(yAxis).get(z);
            if (zSpace.equals("")) {
                zPath.add(z);
            } else {
                return zPath;
            }
        }
        return zPath;

    }

    /**
     * Start search from start points
     * @param startPoint
     * @return
     */
    private static Point findFirstFreeZ(Point startPoint){
        //find first free z
        int xAxis = startPoint.x;
        int yAxis = startPoint.y;
        int zAxis = startPoint.z;
        for (int x = xAxis; x < x_axis_length; x++) {
            xAxis = x;
            for (int y = yAxis; y < y_axis_length; y++) {
                yAxis = y;
                for (int z = zAxis; z < z_axis_length; z++) {
                    zAxis = z;
                    String zSpace = space.get(x).get(y).get(z);
                    if (zSpace == "") {
                        return new Point(xAxis, yAxis, zAxis);
                    }
                }
            }
        }
        return new Point(xAxis, yAxis, zAxis);
    }

    /**
     * Start search from zero
     * @return
     */
    private static Point findFirstFreeZ(){
        return findFirstFreeZ(new Point(0, 0, 0));
    }
}
