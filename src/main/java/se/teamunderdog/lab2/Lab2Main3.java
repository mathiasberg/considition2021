package se.teamunderdog.lab2;

import org.apache.commons.math3.geometry.Point;
import org.jgrapht.alg.drawing.model.Points;
import se.teamunderdog.Main;
import se.teamunderdog.lab1.Worker;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.starterkit.GameLayer;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Lab2Main3 {
    se.teamunderdog.starterkit.models.responses.GameResponse gameInformation;
    int x_axis_length = 10;
    int y_axis_length = 10;
    int z_axis_length = 10;
    ArrayList<ArrayList<ArrayList<String>>> space;
    List<PackageInfo> loaded = new ArrayList<>();
    List<PackageInfo> notLoaded = new ArrayList<>();

    public static void main(String[] args) {
        GameLayer gameLayer = new GameLayer(Main.ApiKey);
        se.teamunderdog.starterkit.models.responses.GameResponse gameInformation = gameLayer.newGame(Main.Map, Main.ApiKey);
        Lab2Main3 main = new Lab2Main3();
        main.start(gameInformation);
    }

    private void start(se.teamunderdog.starterkit.models.responses.GameResponse gameInformation) {
        this.gameInformation = gameInformation;
        x_axis_length = gameInformation.vehicle.length;
        y_axis_length = gameInformation.vehicle.width;
        z_axis_length = gameInformation.vehicle.height;

        Worker worker = new WorkerLab2(gameInformation);
        worker.startSorting();
        List<PackageInfo> packageList = worker.createPackageList();

        space = new ArrayList<>(x_axis_length);

        for (int x = 0; x < x_axis_length; x++) {
            space.add(new ArrayList<ArrayList<String>>(y_axis_length));
            for (int y = 0; y < y_axis_length; y++) {
                space.get(x).add(new ArrayList<String>(z_axis_length));
                for (int z = 0; z < z_axis_length; z++) {
                    //space.get(x).get(y).add(new ArrayList<String>(z_axis_length));
                    space.get(x).get(y).add(z, "");
                }
            }
        }

        Rectangle rectangle = new Rectangle(0, 0, 2, 2);
        //Points.add()
        //Points

    }
}
