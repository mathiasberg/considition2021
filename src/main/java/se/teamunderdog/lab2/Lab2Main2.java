package se.teamunderdog.lab2;

import se.teamunderdog.Main;
import se.teamunderdog.lab1.Worker;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.starterkit.GameLayer;
import se.teamunderdog.starterkit.models.Package;
import se.teamunderdog.starterkit.models.responses.GameResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Lab2Main2 {

    GameResponse gameInformation;
     int x_axis_length = 10;
     int y_axis_length = 10;
     int z_axis_length = 10;
     ArrayList<ArrayList<ArrayList<Point>>> space;
     List<PackageInfo> loaded = new ArrayList<>();
     List<PackageInfo> notLoaded = new ArrayList<>();

    public static void main(String[] args) {
        GameLayer gameLayer = new GameLayer(Main.ApiKey);
        se.teamunderdog.starterkit.models.responses.GameResponse gameInformation = gameLayer.newGame(Main.Map, Main.ApiKey);
        Lab2Main2 main = new Lab2Main2();
        main.start(gameInformation);
    }

    private void start(GameResponse gameInformation) {
        this.gameInformation = gameInformation;
        x_axis_length = gameInformation.vehicle.length;
        y_axis_length = gameInformation.vehicle.width;
        z_axis_length = gameInformation.vehicle.height;

        Worker worker = new WorkerLab2(gameInformation);
        worker.startSorting();
        List<PackageInfo> packageList = worker.createPackageList();

        space = new ArrayList<>(x_axis_length);

        for (int x = 0; x < x_axis_length; x++) {
            space.add(new ArrayList<ArrayList<Point>>(y_axis_length));
            for (int y = 0; y < y_axis_length; y++) {
                space.get(x).add(new ArrayList<Point>(z_axis_length));
                for (int z = 0; z < z_axis_length; z++) {
                    //space.get(x).get(y).add(new ArrayList<String>(z_axis_length));
                    space.get(x).get(y).add(z,null);
                }
            }
        }

        Point prevPoint = new Point(0, 0, 0);
        while(loaded.size() < gameInformation.dimensions.size()){
            //next free spot
            Point parentToFirstFreeZ = findParentToFirstFreeZ(prevPoint);
            PackageInfo parentPackage = null;
            if(parentToFirstFreeZ != null){
                parentPackage = findPackage(parentToFirstFreeZ.id);
            }
            Point firstFreeZAlt2 = findFirstFreeZAlt2(prevPoint);
            Point firstFreeZ = findFirstFreeZ(prevPoint);
            System.out.println("firstFreeZ = " + firstFreeZ);
            System.out.println("prevPoint = " + prevPoint);
            for (PackageInfo p : packageList) {
                if (loaded.contains(p)){
                    continue;
                }
                if (isPathFilled(firstFreeZ, p)) {
                    if (isRotatedPathZToYFilled(firstFreeZ, p)) {
                        if (isRotatedPathZToXFilled(firstFreeZ, p)) {
                            continue;
                        }
                    }
                }
                loaded.add(p);

                fillSpace(p, firstFreeZ);
                break;

                //packet fit on xyz
                    //package is lighter then package below

                //package not fit xyz. try rotate

                //add to load

                //fill map
            }
            prevPoint = firstFreeZ;

        }

        System.out.println("loaded = " + loaded);


    }

    private PackageInfo findPackage(Integer id) {
        for (Package p : gameInformation.dimensions) {
            if(p.id == id){
                return new PackageInfo(p);
            }
        }
        return null;
    }

    private void fillSpace(PackageInfo pInfo, Point start) {
        Package p = pInfo.getRotatedPackage() == null ? pInfo.getPackage() : pInfo.getRotatedPackage();
        Point parent = null;
        for (int x = start.x; x < start.x + p.length; x++) {
            for (int y = start.y; y < start.y + p.width; y++) {
                for (int z = start.z; z < start.z + p.height; z++) {
                    Point point = new Point(p.id, x, y, z, parent);
                    space.get(x).get(y).add(z, point);
                    parent = point;
                }
            }
        }
    }

    private boolean isPathFilled(Point start, PackageInfo p){
        Point end;
        if(start.parent != null){
            end = new Point((start.parent.x + p.getLength()), (start.parent.y + p.getWidth()), (start.parent.z + p.getHeight()));
        } else {
            end = new Point((start.x + p.getLength()), (start.y + p.getWidth()), (start.z + p.getHeight()));
        }

        if(fitOnXYZAxis(start, end)) {
            List<Point> path = findPath(start, end);
            return path.stream()
                    .anyMatch(Objects::nonNull);
        }
        return false;

    }

    private boolean isRotatedPathZToYFilled(Point start, PackageInfo p){
        Package rp = new Package(p.getPackage().id, p.getLength(), p.getHeight(), p.getWidth(),
                p.getPackage().weightClass, p.getPackage().orderClass);

        Point end = new Point((start.x + rp.length), (start.y + rp.width), (start.z + rp.height));
        if(fitOnXYZAxis(start, end)) {
            List<Point> path = findPath(start, end);
            boolean xContains = path.stream()
                    .anyMatch(Objects::nonNull);
            if (!xContains){
                p.setRotatedPackage(rp);
            }
            return xContains;
        }
        return false;

    }

    private boolean isRotatedPathZToXFilled(Point start, PackageInfo p){
        Package rp = new Package(p.getPackage().id, p.getHeight(), p.getWidth(), p.getLength(),
                p.getPackage().weightClass, p.getPackage().orderClass);

        Point end = new Point((start.x + rp.length), (start.y + rp.width), (start.z + rp.height));
        if(fitOnXYZAxis(start, end)) {
            List<Point> path = findPath(start, end);
            boolean xContains = path.stream()
                    .anyMatch(Objects::nonNull);
            if (!xContains){
                p.setRotatedPackage(rp);
            }
            return xContains;
        }
        return false;
    }

    private List<Point> findPath(Point start, Point end){
        List<Point> zPath = new ArrayList<>();
        int length = start.x + end.x;
        int width = start.y + end.y;
        int height = start.z + end.z;
        for (int x = start.x; x < length; x++) {
            for (int y = start.y; y < width; y++) {
                for (int z = start.z; z < height; z++) {
                    zPath.add(space.get(x).get(y).get(z));
                }
            }
        }

        return zPath;

    }

    private boolean fitOnXYZAxis(Point point, Point end){

        boolean xAxisFit = point.x + end.x <= x_axis_length;
        boolean yAxisFit = point.y + end.y <= y_axis_length;
        boolean zAxisFit = point.z + end.z <= z_axis_length;
        return xAxisFit && yAxisFit && zAxisFit;

    }

    private boolean fitOnXYZAxis(Point point, PackageInfo p){
        Package packageToLoad = p.getRotatedPackage() == null ? p.getPackage() : p.getRotatedPackage();
        return fitOnXYZAxis(point, new Point(packageToLoad));
    }

    /**
     * Start search from start points
     * @param startPoint
     * @return
     */
    private Point findFirstFreeZ(Point startPoint){
        //find first free z
        Point firstFree = null;
        Point parent = null;
        int xAxis = 0, yAxis = 0, zAxis = 0;
        for (int x = startPoint.x; x < x_axis_length; x++) {
            for (int y = startPoint.y; y < y_axis_length; y++) {
                for (int z = startPoint.z; z < z_axis_length; z++) {
                    Point zSpace = space.get(x).get(y).get(z);
                    if (zSpace == null) {
                        firstFree = new Point(-1, x, y, z, new Point(xAxis-1, yAxis-1, zAxis-1));
                        break;
                    }
                    if (parent == null || zSpace.id != parent.id) {
                        PackageInfo info = findPackage(zSpace.id);
                        xAxis += info.getLength();
                        yAxis += info.getWidth();
                        zAxis += info.getHeight();
                    }
                    parent = zSpace;
                }
            }
        }
        if (firstFree != null) {
            PackageInfo parentInfo = findPackage(firstFree.parent.id);

        }

        return null;
    }

    private Point findFirstFreeZAlt2(Point startPoint){
        //find first free z
        int maxZ= -1;
        int maxY= -1;
        int maxX= -1;
        int xAxis = 0, yAxis = 0, zAxis = 0;
        for (int x = startPoint.x; maxX == -1 && x < x_axis_length; x++) {
            if(maxY != -1){
                Point zSpace = space.get(x).get(maxY).get(maxZ);
                if (zSpace == null && maxX == -1) {
                    maxX = x;
                }
            }

            for (int y = startPoint.y; maxY == -1 && y < y_axis_length; y++) {
                if(maxZ != -1){
                    Point zSpace = space.get(x).get(y).get(maxZ);
                    if (zSpace == null && maxY == -1) {
                        maxY = y;
                    }
                }

                for (int z = startPoint.z; maxZ == -1 && z < z_axis_length; z++) {
                    Point zSpace = space.get(x).get(y).get(z);
                    if (zSpace == null && maxZ == -1) {
                        maxZ = z;
                    }
                }
            }
        }

        return null;
    }

    private Point findFirstFreeY(Point startPoint){
        //first free x
        Point parent = null;
        for (int x = startPoint.x; x < x_axis_length; x++) {
            for (int y = startPoint.y; y < y_axis_length; y++) {
                Point point = space.get(x).get(y).get(startPoint.z);
                if (point == null) {
                    return new Point(-1, x, y, startPoint.z, parent);
                }
                parent = point;
            }

        }


        return null;
    }

    private Point findFirstFreeX(Point startPoint){
        //first free x
        Point parent = null;
        for (int x = startPoint.x; x < x_axis_length; x++) {
            for (int y = startPoint.y; y < y_axis_length; y++) {
                Point point = space.get(x).get(y).get(startPoint.z);
                if (point == null) {
                    return new Point(-1, x, y, startPoint.z, parent);
                }
                parent = point;
            }

        }


        return null;
    }

    private Point findParentToFirstFreeZ(Point startPoint){
        //find first free z
        Point parent = null;
        int xAxis = 0, yAxis = 0, zAxis = 0;
        for (int x = startPoint.x; x < x_axis_length; x++) {
            for (int y = startPoint.y; y < y_axis_length; y++) {
                for (int z = startPoint.z; z < z_axis_length; z++) {
                    Point zSpace = space.get(x).get(y).get(z);
                    if (zSpace == null) {
                        if (parent != null) {
                            PackageInfo parentInfo = findPackage(parent.id);
                            //new Point(parentInfo.getPackage().id, x, y, z, null);
                            parent = new Point(parent.id, xAxis-1, yAxis-1, zAxis-1, null);
                        }
                        return parent;
                    }
                    if (parent == null || zSpace.id != parent.id) {
                        PackageInfo info = findPackage(zSpace.id);
                        xAxis += info.getLength();
                        yAxis += info.getWidth();
                        zAxis += info.getHeight();
                    }
                    parent = zSpace;
                }
            }
        }
        return parent;
    }
}
