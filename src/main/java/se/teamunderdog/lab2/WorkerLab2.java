package se.teamunderdog.lab2;

import se.teamunderdog.lab1.Worker;
import se.teamunderdog.model.PackageInfo;
import se.teamunderdog.starterkit.models.responses.GameResponse;

import java.util.Collections;
import java.util.List;

public class WorkerLab2 extends Worker {
    public WorkerLab2(GameResponse gameInformation) {
        super("WorkerLab2", gameInformation);
    }

    public void startSorting() {
        this.sorter = compareByMaxArea;
        System.out.println("");

        Collections.sort(getOrderE(), sorter);
        Collections.sort(getOrderD(), sorter);
        Collections.sort(getOrderC(), sorter);
        Collections.sort(getOrderB(), sorter);
        Collections.sort(getOrderA(), sorter);

        //sort by order prio - highest (A=0) should be last packed

        //sort each order class by volume

        //sort by weight - heaviest should be first in each row


    }

    public List<PackageInfo> createPackageList() {

        System.out.println("");
        getCompletePackList().addAll(getOrderE());
        getCompletePackList().addAll(getOrderD());
        getCompletePackList().addAll(getOrderC());
        getCompletePackList().addAll(getOrderB());
        getCompletePackList().addAll(getOrderA());

        return getCompletePackList();
    }
}
