package se.teamunderdog;

import se.teamunderdog.lab1.Worker;
import se.teamunderdog.model.VehicleInfo;

public class PrintStat {

    public static void printBefore(se.teamunderdog.starterkit.models.responses.GameResponse gameResponse, VehicleInfo vehicleInfo, Worker worker) {
        System.out.println("====== Map INFO =======");
        System.out.println("Name " + gameResponse.mapName);

        System.out.println("====== VEHICLE INFO =======");
        System.out.println("Volume " + vehicleInfo.getVolume());
        System.out.println("Width (x) " + vehicleInfo.getWidth());
        System.out.println("Length (y) " + vehicleInfo.getLength());
        System.out.println("Height (z) " + vehicleInfo.getHeight());

        System.out.println("====== Packages INFO =======");
        System.out.println("Total volume " + worker.getTotalPackagesVolume());
        System.out.println("# " + gameResponse.dimensions.size());
        System.out.println("# A " + worker.getOrderA().size() );
        System.out.println("# B " + worker.getOrderB().size() );
        System.out.println("# C " + worker.getOrderC().size() );
        System.out.println("# D " + worker.getOrderD().size() );
        System.out.println("# E " + worker.getOrderE().size() );
        System.out.println("# Heavy " + worker.getWeightHeavy().size());
        System.out.println("# Medium " + worker.getWeightMedium().size());
        System.out.println("# Light " + worker.getWeightLight().size());

        System.out.println("====== Worker INFO =======");
        System.out.println("name= " + worker.getName());

        System.out.println("==========================================");
    }

    public static void printAfter(){

    }

}
