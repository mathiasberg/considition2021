package se.teamunderdog;

import se.teamunderdog.starterkit.models.Package;

public class RotatePackageUtil {
    public static Package rotateZtoY(Package p) {
        Package rp = new Package(p.id, p.length, p.height, p.width,
                p.weightClass, p.orderClass);

        return rp;
    }

    public static Package rotateZtoX(Package p){
        Package rp = new Package(p.id, p.height, p.width, p.length,
                p.weightClass, p.orderClass);
        return rp;
    }
}
